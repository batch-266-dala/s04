package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");

        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getBreed());


        Dog adoptedPet = new Dog();
        adoptedPet.setName("Lala");
        adoptedPet.setColor("White Brown");
        adoptedPet.setBreed("Huskal");

        adoptedPet.speak();
        System.out.println(adoptedPet.getName() + " is a " + adoptedPet.getColor() + " " + adoptedPet.getBreed());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);

        System.out.println("Car name: " + myCar.getName());
        System.out.println("Brand: " + myCar.getBrand());
        System.out.println("Year Produced: " + myCar.getYearOfMake());
        System.out.println("Car driver: " + myCar.getDriverName());

//        Abstraction
//        -is a process where all the logic and complexity are hidden from the user
        Person person = new Person();
        person.sleep();
        person.run();

//        Polymorphism
//        Delivered from the greek word: poly means "many" and morph means "forms"
        StaticPoly poly = new StaticPoly();
        System.out.println(poly.addition(7,2));
        System.out.println(poly.addition(7,2, 3));
        System.out.println(poly.addition(7.2,2.6));
    }
}
