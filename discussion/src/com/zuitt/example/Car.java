package com.zuitt.example;

public class Car {
//        Object-Oriented Concepts
//        Here are the definitions of the following:
//        Object - An abstract idea in your mind that represents something in the real world
//        Example: The concept of a dog
//        Class - The representation of the object using code
//        Example: Writing code that would describe a dog
//        Instance - A unique copy of the idea, made "physical"

//        Objects
//        Object are composed of two components
        /*
        * 1. States and Attributes - what is the idea about?
        * 2. Behaviours - what can idea do?
        *
        * Example: A person has attributes like name, age, height and weight. And a person can eat, sleep, and speak.
        * */

//        Class Creation
        /*
        * A class is composed of four parts:
        * 1. Properties - characteristics of the object
        * 2. Constructors - used to create an objects
        * 3. Getters/Setters - get and set the values of each property of the object
        * 4. Methods - functions that an object can perform
        * */

//        Properties
        private String name;
        private String brand;
        private int yearOfMake;

//        Constructors
        /*Empty Constructor*/
//        private Car(){}
        /*Parameterized Constructor*/
        private Car(String carName, String carBrand, int carYearOfMake){
            this.name = carName;
            this.brand = carBrand;
            this.yearOfMake = carYearOfMake;
        }

//      Setters
    public void setName(String carName){
            this.name = carName;
    }

    public void setBrand(String carBrand){
        this.brand = carBrand;
    }

    public void setYearOfMake(int carYearOfMake){
        this.yearOfMake = carYearOfMake;
    }

//    Getters
    public String getName(){
            return this.name;
    }

    public String getBrand(){
            return this.brand;
    }

    public int getYearOfMake(){
            return this.yearOfMake;
    }

//    Methods
    public void Drive(){
        System.out.println("The car is running.");
    }

//    Access Modifiers
    /*1. default - no keyword required
    * 2. private - only accessible within the class
    * 3. protected - only accessible to/with the classes
    * 4. public - can be accessed anywhere
    * */

//    Fundamentals of OOP
    /*1. Encapsulation - mechanism of wrapping data (variables) and code acting on the data.
    * 2. Inheritance - objects or properties can be shared to sub-classes
    * 3. Abstraction - Way of using functions, process, codes without knowing its origin.
    * 4. Polymorphism - Passing of attributes and can be added or modified.
    *  */

    /*Make Driver a component of car*/

    private Driver d;

    public Car(){
        this.d = new Driver("Alejandro"); //Whenever a new car is created, it will have a driver named Alejandro.
    }

    public String getDriverName(){
        return this.d.getName();
    }

}
