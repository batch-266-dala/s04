package com.zuitt.example;

public class Dog extends Animal {
    private String breed;

    public Dog(){
        super(); // Animal() Constructor
        this.breed = "Chihuahua";
    }
    public Dog(String name, String color, String breed)  {
        super(name, color);
        this.breed = breed;
    }

    public String getBreed(){
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void speak(){
        super.call();
        System.out.println("woof, woof!");
    }
}
